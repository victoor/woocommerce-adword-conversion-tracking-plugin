=== Plugin Name ===
Contributors: victoor89
Donate link: http://victorfalcon.es/
Tags: woocommerce, google adwords, conversion, tracking, code
Requires at least: 3.8
Tested up to: 3.8.1
Stable tag: 3.8.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add the Google Adwords conversión code to the woocommerce thankyou page. 

== Description ==

Add your **Google Adwords** conversion code to the Woocommerce Thank You page easily. ¡Just in One Second!

Also it send the value of the order to Google Adwords so you can track your earnings.

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to your Woocommerce Integrations Setings page

== Frequently Asked Questions ==



= What about foo bar? =



== Screenshots ==

Not yet

== Changelog ==

= 1.0 =
* Added settings page to configure your conversion label and conversion id
* Added the conversion code to the thankyou page
* Added the value of the order to the conversion value field

== Upgrade Notice ==

= 1.0 =
Stable version.
